﻿using Newtonsoft.Json;
using System;
using System.Data.SQLite;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OsrsBatch2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await BulkGeUpdate();
        }
        private static async Task<bool> BulkGeUpdate()
        {
            var Start = DateTime.Now;
            Console.WriteLine("BulkGeUpdate started at " + Start.ToString("yyyy-MM-dd HH:mm:ss"));
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=/db/rsitems.db;Version=3;");
            conn.Open();
            string letters = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,#";
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://services.runescape.com/m=itemdb_oldschool/api/");
                foreach (string letter in letters.Split(","))
                {
                    int page = 1;
                    while (true)
                    {
                        Console.WriteLine($"Retrieving for letter {letter} page {page}.");
                        var response = await client.GetAsync($"catalogue/items.json?category=1&alpha={Uri.EscapeDataString(letter)}&page={page++}");
                        if (!response.IsSuccessStatusCode)
                        {
                            Console.WriteLine($"Exiting with error code {response.StatusCode} for reason {response.ReasonPhrase}");
                            return false;
                        }
                        string json = await response.Content.ReadAsStringAsync();
                        dynamic items = JsonConvert.DeserializeObject<dynamic>(json);
                        if (items == null || items.items == null)
                        {
                            Console.WriteLine($"Items was null for letter {letter} page {page}, trying again in 10 seconds.");
                            page--;
                            Thread.Sleep(20000);
                            continue;
                        } 
                        var itemArray = items.items;
                        Thread.Sleep(3000);
                        int numFound = 0;
                        foreach (var item in itemArray)
                        {
                            var cmd = conn.CreateCommand();
                            string members = item.members == "true" ? "1" : "0";
                            string formattedPrice = ((string)item.current.price).Replace(",", "").Replace(".", "").Replace("k", "00").Replace("m", "00000").Replace("b", "00000000");
                            cmd.CommandText = $"SELECT RS_ID FROM ITEM WHERE RS_ID = {item.id} LIMIT 1";
                            var reader = cmd.ExecuteReader();
                            bool exists = reader.Read();
                            reader.Close();
                            if(exists) 
                                cmd.CommandText = $"UPDATE ITEM SET ICON = '{item.icon}', ICON_LARGE = '{item.icon_large}', TYPE = '{item.type}', TYPE_ICON = '{item.typeIcon}', NAME = '{((string)item.name).Replace("'", "''")}', DESCRIPTION = '{((string)item.description).Replace("'", "''")}', PRICE = {formattedPrice}, MEMBERS = {members} WHERE RS_ID = {item.id}";
                            else
                                cmd.CommandText = $"INSERT INTO ITEM (ICON, ICON_LARGE, RS_ID, TYPE, TYPE_ICON, NAME, DESCRIPTION, PRICE, MEMBERS) VALUES ('{item.icon}', '{item.icon_large}', {item.id}, '{item.type}', '{item.typeIcon}', '{((string)item.name).Replace("'", "''")}', '{((string)item.description).Replace("'", "''")}', {formattedPrice}, {members})";
                            cmd.ExecuteNonQuery();
                            numFound++;
                        }
                        if (numFound == 0)
                            break;
                    }
                }
            }
            conn.Dispose();
            var End = DateTime.Now;
            Console.WriteLine("BulkGeUpdate finished at " + End.ToString("yyyy-MM-dd HH:mm:ss"));
            return true;
        }
    }
}
